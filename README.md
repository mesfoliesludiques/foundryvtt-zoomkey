## ZoomKey
Adds hotkeys for zooming and panning on the canvas without mousewheel

- Zooming in: Page Up or Ctrl + '+'
- Zooming out: Page Down or Ctrl + '-'
- Panning : Ctrl + arrows